package mikhail.yudin.GoogleImageSearcher.image.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.*;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import mikhail.yudin.GoogleImageSearcher.image.Image;
import mikhail.yudin.GoogleImageSearcher.utils.ImageUtils;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 28.02.14
 * Time: 23:13
 * To change this template use File | Settings | File Templates.
 */
public class ImageArrayAdapter extends ArrayAdapter<Image> {
    private static final float IMAGE_SIDE_SIZE_IN_DP = 150f;
    public static final int INITIAL_VALUE = -1;

    private final int imageSideSizeInPx;
    private final View.OnClickListener onClickListener;

    /**
     * Current page loaded from google (8 is max in free mode accessing)
     */
    private int currentPage = INITIAL_VALUE;

    /**
     * Maximum amount of pages found by google (from 1 to 8).
     */
    private int maxPageCount = INITIAL_VALUE;

    public ImageArrayAdapter(Context context, List<Image> images) {
        super(context, 0, images);
        this.imageSideSizeInPx = Math.round(IMAGE_SIDE_SIZE_IN_DP * context.getResources().getDisplayMetrics().density);
        this.onClickListener = new OnImageClickListener();
        super.setNotifyOnChange(false);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Image imageInfo = this.getItem(position);
        final ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(getContext());
            GridView.LayoutParams layoutParams = new GridView.LayoutParams(imageSideSizeInPx, imageSideSizeInPx);
            imageView.setLayoutParams(layoutParams);
            imageView.setOnClickListener(onClickListener);
        } else {
            imageView = (ImageView) convertView;
            imageView.setImageBitmap(null);
        }
        imageView.setTag(imageInfo);
        ImageUtils.loadThumbImage(imageInfo, imageView);
        return imageView;
    }

    public void addAll(List<Image> data) {
        if (maxPageCount == INITIAL_VALUE) {
            throw new IllegalStateException("Maximum page count can't have a value of " + INITIAL_VALUE);
        }
        currentPage++;
        currentPage = currentPage % maxPageCount;
        currentPage = currentPage == maxPageCount - 1 ? INITIAL_VALUE : currentPage;
        for (Image image : data) {
            super.add(image);
        }
    }

    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public void clear() {
        super.clear();
        currentPage = INITIAL_VALUE;
        maxPageCount = INITIAL_VALUE;
    }

    @Override
    public void add(final Image object) {
        throw new UnsupportedOperationException("Operation is unsupported");
    }

    @Override
    public void insert(final Image object, final int index) {
        throw new UnsupportedOperationException("Operation is unsupported");
    }

    @Override
    public void remove(final Image object) {
        throw new UnsupportedOperationException("Operation is unsupported");
    }

    public void setMaxPageCount(final int maxPageCount) {
        this.maxPageCount = maxPageCount;
    }

    public int getMaxPageCount() {
        return maxPageCount;
    }

    public void setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
    }
}
