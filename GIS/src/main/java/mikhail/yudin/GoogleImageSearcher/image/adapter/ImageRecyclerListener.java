package mikhail.yudin.GoogleImageSearcher.image.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 28.02.14
 * Time: 23:20
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
public class ImageRecyclerListener implements AbsListView.RecyclerListener {
    @Override
    public void onMovedToScrapHeap(final View view) {
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            BitmapDrawable drawable = ((BitmapDrawable) imageView.getDrawable());
            if (drawable != null) {
                Bitmap bitmap = drawable.getBitmap();
                if (bitmap != null) {
                    bitmap.recycle();
                    Log.e("recycled", bitmap.toString());
                }
            }
            imageView.setImageBitmap(null);
        }
    }
}
