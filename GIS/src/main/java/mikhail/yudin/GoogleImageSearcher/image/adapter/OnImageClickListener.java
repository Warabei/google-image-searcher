package mikhail.yudin.GoogleImageSearcher.image.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import mikhail.yudin.GoogleImageSearcher.activity.ImageViewActivity;
import mikhail.yudin.GoogleImageSearcher.image.Image;

/**
 * Created by fuck3r on 04.03.14.
 */
class OnImageClickListener implements View.OnClickListener {
    @Override
    public void onClick(final View view) {
        final Image image = (Image) view.getTag();
        final Context context = view.getContext();
        final Intent intent = new Intent(context, ImageViewActivity.class);
        intent.putExtra(Image.IMAGE, image);
        context.startActivity(intent);
    }
}
