package mikhail.yudin.GoogleImageSearcher.image;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 28.02.14
 * Time: 22:30
 * To change this template use File | Settings | File Templates.
 */
public final class Image implements Parcelable {
    public static final String IMAGE = "Image.Image";

    public static List<Image> parseJsonArray(JsonArray imagesJsonArray) {
        ArrayList<Image> results = new ArrayList<Image>();
        Gson gson = new Gson();
        for (int i = 0; i < imagesJsonArray.size(); i++) {
            try {
                results.add(gson.fromJson(imagesJsonArray.get(i), Image.class));
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return results;
    }

    /**
     * It is needed for GSON
     */
    public Image() {
    }

    private String url;

    @SerializedName("tbUrl")
    private String thumbUrl;

    @SerializedName("imageId")
    private String localPath;

    public String getUrl() {
        return url;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public String getThumbLocalPath() {
        return localPath + "_thumb";
    }

    public String getFullLocalPath() {
        return localPath + "_full";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int i) {
        parcel.writeString(url);
        parcel.writeString(thumbUrl);
        parcel.writeString(localPath);
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    private Image(Parcel parcel) {
        url = parcel.readString();
        thumbUrl = parcel.readString();
        localPath = parcel.readString();
    }
}
