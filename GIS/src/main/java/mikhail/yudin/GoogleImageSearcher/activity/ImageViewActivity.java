package mikhail.yudin.GoogleImageSearcher.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import mikhail.yudin.GoogleImageSearcher.R;
import mikhail.yudin.GoogleImageSearcher.image.Image;
import mikhail.yudin.GoogleImageSearcher.image.ScalableImageView;
import mikhail.yudin.GoogleImageSearcher.utils.ImageUtils;

/**
 * Created by fuck3r on 04.03.14.
 */
public class ImageViewActivity extends ActionBarActivity {

    private static final float MAX_ZOOM = 8f;
    private ScalableImageView imageView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_img_view);
        setupViews();
        init();
    }

    private void init() {
        imageView.setMaxZoom(MAX_ZOOM);
        ImageUtils.loadFullImage((Image) getIntent().getParcelableExtra(Image.IMAGE), imageView);
    }

    private void setupViews() {
        imageView = (ScalableImageView) findViewById(R.id.img_view);
    }

}
