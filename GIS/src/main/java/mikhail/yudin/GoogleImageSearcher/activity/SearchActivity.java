package mikhail.yudin.GoogleImageSearcher.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import mikhail.yudin.GoogleImageSearcher.R;
import mikhail.yudin.GoogleImageSearcher.image.Image;
import mikhail.yudin.GoogleImageSearcher.image.adapter.EndlessScrollListener;
import mikhail.yudin.GoogleImageSearcher.image.adapter.ImageArrayAdapter;
import mikhail.yudin.GoogleImageSearcher.image.adapter.ImageRecyclerListener;
import mikhail.yudin.GoogleImageSearcher.search.GoogleSearchUpdater;
import mikhail.yudin.GoogleImageSearcher.search.UISearchUpdaterHandler;

import java.util.ArrayList;

public class SearchActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private static final String IMAGES = "images";
    private static final String GIS_PREFERENCES = "gis_preferences";
    private static final String CURRENT_PAGE = "current_page";
    private static final String MAX_PAGE = "max_page";
    private static final String IS_LOADING = "is_loading";
    private static final String CURRENT_QUERY = "current_query";
    private static final long ONE_SECOND = 1000L;

    private GridView imagesGridView;
    private ArrayList<Image> images;
    private ImageArrayAdapter imageArrayAdapter;
    private String currentQuery = "";
    private EndlessScrollListener endlessScrollListener;
    private long lastSearchClickTime;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_search);
        setupViews();
        restoreState(savedInstanceState);
        startSearch(savedInstanceState);
        lastSearchClickTime = System.nanoTime();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveQuery();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(IMAGES, images);
        outState.putInt(CURRENT_PAGE, imageArrayAdapter.getCurrentPage());
        outState.putInt(MAX_PAGE, imageArrayAdapter.getMaxPageCount());
        outState.putBoolean(IS_LOADING, endlessScrollListener.isLoading());
        outState.putString(CURRENT_QUERY, currentQuery);
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        //To avoid overclicking
        if (checkIfSecondPassedBetweenTheQueries()) {
            imageArrayAdapter.clear();
            imageArrayAdapter.notifyDataSetChanged();
            currentQuery = query;
            search();
        }
        return true;
    }

    private boolean checkIfSecondPassedBetweenTheQueries() {
        long currentTime = System.nanoTime();
        boolean isSecondPassed = currentTime - lastSearchClickTime > ONE_SECOND;
        lastSearchClickTime = currentTime;
        return isSecondPassed;
    }

    @Override
    public boolean onQueryTextChange(final String query) {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint(getString(R.string.search));
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
        searchView.setQuery(currentQuery, false);
        return true;
    }

    private void startSearch(final Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.getBoolean(IS_LOADING)) {
            search();
        }
    }

    private void restoreState(final Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            images = new ArrayList<Image>();
            initQuery();
            imagesGridView.setAdapter(imageArrayAdapter = new ImageArrayAdapter(this, images));
        } else {
            images = savedInstanceState.getParcelableArrayList(IMAGES);
            imagesGridView.setAdapter(imageArrayAdapter = new ImageArrayAdapter(this, images));
            imageArrayAdapter.setCurrentPage(savedInstanceState.getInt(CURRENT_PAGE, ImageArrayAdapter.INITIAL_VALUE));
            imageArrayAdapter.setMaxPageCount(savedInstanceState.getInt(MAX_PAGE, ImageArrayAdapter.INITIAL_VALUE));
            currentQuery = savedInstanceState.getString(CURRENT_QUERY);
        }
        endlessScrollListener = new ImageEndlessScrollListener();
        imagesGridView.setOnScrollListener(endlessScrollListener);
        //imagesGridView.setRecyclerListener(new ImageRecyclerListener());
    }

    private void saveQuery() {
        SharedPreferences settings = getSharedPreferences(GIS_PREFERENCES, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(CURRENT_QUERY, currentQuery);
        editor.commit();
    }

    private void initQuery() {
        SharedPreferences settings = getSharedPreferences(GIS_PREFERENCES, 0);
        currentQuery = settings.getString(CURRENT_QUERY, currentQuery);
    }

    private void setupViews() {
        imagesGridView = (GridView) findViewById(R.id.image_grid_view);
    }

    private void search() {
        String query = GoogleSearchUpdater.getSearchQuery(imageArrayAdapter.getCurrentPage() + 1, currentQuery);
        Handler handler = new UISearchUpdaterHandler(endlessScrollListener, imageArrayAdapter);
        GoogleSearchUpdater.requestUpdates(query, handler);
    }

    private class ImageEndlessScrollListener extends EndlessScrollListener {
        @Override
        public void onLoadMore(final int page, final int totalItemsCount) {
            search();
        }
    }


}
