package mikhail.yudin.GoogleImageSearcher;

import android.app.Application;
import mikhail.yudin.GoogleImageSearcher.cache.DiskLimitedLruCache;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 01.03.14
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */
public class GisApplication extends Application {

    private static final int CAPACITY_LIMIT = 20 * 1024 * 1024;

    @Override
    public void onCreate() {
        super.onCreate();
        DiskLimitedLruCache.getInstance().init(getExternalCacheDir(), CAPACITY_LIMIT);
    }

}
