package mikhail.yudin.GoogleImageSearcher.cache;

import android.util.Log;

import java.io.File;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Lock-free cache with weak guarantees. We can expect cache size will be
 * near the desirable capacity. But it allows to avoid contentions.
 */
public class DiskLimitedLruCache implements Cacheable {

    private static final long MIN_CACHE_CAPACITY = 5L * 1024L * 1024L;
    private final AtomicLong cacheCapacity;
    private final Set<File> fileCache;
    private File cacheFolder;
    private long capacityLimit;

    private DiskLimitedLruCache() {
        this.cacheCapacity = new AtomicLong();
        this.fileCache = Collections.newSetFromMap(new ConcurrentHashMap<File, Boolean>());
    }

    public static DiskLimitedLruCache getInstance() {
        return CacheHolder.INSTANCE;
    }

    public synchronized void init(final File cacheFolder, final long capacityLimit) {
        if (cacheFolder == null) {
            throw new IllegalArgumentException("Cache folder can't be null");
        }
        if (capacityLimit < MIN_CACHE_CAPACITY) {
            throw new IllegalArgumentException("Capacity limit is too small.");
        }

        if (this.cacheFolder == null) {
            this.capacityLimit = capacityLimit;
            this.cacheFolder = cacheFolder;
            cacheFolder.mkdirs();
            new Thread(new CacheFillingTask()).start();
        } else {
            Log.w("diskLRUCache", "Trying to re-init");
        }
    }

    @Override
    public boolean put(final File file) {
        if (file == null) {
            throw new IllegalArgumentException("File can't be null.");
        }
        long fileSize = file.length();
        file.setLastModified(System.currentTimeMillis());
        if (fileSize > capacityLimit) {
            Log.w("diskLimitCache", "File size is more than threshold value.");
            return false;
        } else {
            if (!fileCache.contains(file)) {
                while (cacheCapacity.get() + fileSize > capacityLimit) {
                    if (!fileCache.isEmpty()) {
                        tryToRemoveOldest();
                    }
                }
            }
            if (fileCache.add(file)) {
                cacheCapacity.addAndGet(fileSize);
            }
            trimToSize();
            return true;
        }
    }

    /**
     * There is no guarantee that file will be available in the future.
     */
    @Override
    public File get(final String key) {
        return new File(cacheFolder, key);
    }

    @Override
    public void clear() {
        fileCache.clear();
        cacheCapacity.set(0L);
        File[] files = cacheFolder.listFiles();
        if (files != null) {
            for (File f : files) {
                tryToDelete(f);
            }
        }
    }

    private void tryToRemoveOldest() {
        long oldestModified = 0L;
        File oldestFile = null;
        for (File file : fileCache) {
            if (oldestFile == null) {
                oldestFile = file;
                oldestModified = file.lastModified();
            } else {
                long freshestModified = file.lastModified();
                if (freshestModified < oldestModified) {
                    oldestModified = freshestModified;
                    oldestFile = file;
                }
            }
        }

        if (oldestFile != null) {
            if (fileCache.remove(oldestFile)) {
                cacheCapacity.addAndGet(-oldestFile.length());
                tryToDelete(oldestFile);
            }
        }

    }

    private void tryToDelete(final File file) {
        try {
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Weak guarantees, cacheCapacity can be modified by other thread.
     * Therefore, it is possible case if some old but redundant files will be deleted occasionally.
     */
    private void trimToSize() {
        while (cacheCapacity.get() > capacityLimit) {
            if (!fileCache.isEmpty()) {
                tryToRemoveOldest();
            }
        }
    }

    private static class CacheHolder {
        private static final DiskLimitedLruCache INSTANCE = new DiskLimitedLruCache();
    }

    private class CacheFillingTask implements Runnable {
        @Override
        public void run() {
            //some performance tuning
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            fillCache();
            trimToSize();
        }

        private void fillCache() {
            File[] cachedFiles = cacheFolder.listFiles();
            if (cachedFiles != null) { // rarely but it can happen, don't know why
                for (File cachedFile : cachedFiles) {
                    if (fileCache.add(cachedFile)) {
                        cacheCapacity.addAndGet(cachedFile.length());
                    }
                }
            } else {
                Log.w("diskLimitCache",
                        "Strange behaviour detected. Cache folder isn't available.");
            }
        }
    }
}
