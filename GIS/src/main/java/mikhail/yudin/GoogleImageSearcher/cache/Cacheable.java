package mikhail.yudin.GoogleImageSearcher.cache;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 27.02.14
 * Time: 21:38
 * To change this template use File | Settings | File Templates.
 */
public interface Cacheable {
    boolean put(File file);

    File get(String key);

    void clear();
}
