package mikhail.yudin.GoogleImageSearcher.search;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mikhail.yudin.GoogleImageSearcher.image.Image;
import mikhail.yudin.GoogleImageSearcher.image.adapter.EndlessScrollListener;
import mikhail.yudin.GoogleImageSearcher.image.adapter.ImageArrayAdapter;

import java.util.List;

/**
 * Created by fuck3r on 04.03.14.
 */
public class UISearchUpdaterHandler extends Handler {
    private final EndlessScrollListener endlessScrollListener;
    private final ImageArrayAdapter imageArrayAdapter;

    public UISearchUpdaterHandler(final EndlessScrollListener endlessScrollListener,
                                  final ImageArrayAdapter imageArrayAdapter) {
        super(Looper.getMainLooper());
        this.endlessScrollListener = endlessScrollListener;
        this.imageArrayAdapter = imageArrayAdapter;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handleMessage(final Message message) {
        if (message.what == GoogleSearchUpdater.ERROR_CODE) {
            endlessScrollListener.invalidateLoading();
        } else {
            imageArrayAdapter.setMaxPageCount(message.what);
            imageArrayAdapter.addAll((List<Image>) message.obj);
            imageArrayAdapter.notifyDataSetChanged();
        }
    }
}
