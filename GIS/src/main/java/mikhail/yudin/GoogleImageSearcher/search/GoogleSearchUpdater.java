package mikhail.yudin.GoogleImageSearcher.search;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import mikhail.yudin.GoogleImageSearcher.image.Image;
import mikhail.yudin.GoogleImageSearcher.utils.NetworkUtils;

import java.lang.ref.SoftReference;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 01.03.14
 * Time: 0:44
 * To change this template use File | Settings | File Templates.
 */
public class GoogleSearchUpdater {

    private static final int MAX_ITEMS_PER_REQUEST = 8;
    public static final int ERROR_CODE = -1;

    private static SoftReference<Thread> currentThread = new SoftReference<Thread>(null);

    public static String getSearchQuery(final int pageNumber, final String query) {
        return "https://ajax.googleapis.com/ajax/services/search/images?rsz=" + MAX_ITEMS_PER_REQUEST + "&"
                + "start=" + pageNumber * MAX_ITEMS_PER_REQUEST + "&v=1.0&q=" + Uri.encode(query);
    }

    public static void requestUpdates(final String query, final Handler queryHandler) {
        //need to be sure invoking goes from main thread to make method unsynchronized
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            throw new IllegalStateException("Called not from UI thread.");
        }

        final Thread thread = currentThread.get();
        if (thread != null) {
            thread.interrupt();
        }

        final Thread newThread = new Thread(new ImageSearchTask(queryHandler, query));
        currentThread = new SoftReference<Thread>(newThread);
        newThread.start();
    }

    private static class ImageSearchTask implements Runnable {

        private final Handler handler;
        private final String query;

        private ImageSearchTask(final Handler handler, final String query) {
            this.handler = handler;
            this.query = query;
        }

        @Override
        public void run() {
            try {
                //some performance tuning
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
                //to avoid overfinding
                //Thread.sleep(200L);

                Log.i("googleSearchQuery", query);
                String responseAsString = NetworkUtils.readAsString(NetworkUtils.sendGetRequest(query));
                Log.i("googleSearchResponse", responseAsString);

                final JsonObject jsonResponse = new JsonParser().parse(responseAsString).getAsJsonObject();
                final JsonObject responseData = jsonResponse.getAsJsonObject("responseData");
                final JsonArray jsonImagesArray = responseData.getAsJsonArray("results");

                final Message message = handler.obtainMessage();
                message.obj = Image.parseJsonArray(jsonImagesArray);
                final JsonArray jsonPages = responseData.getAsJsonObject("cursor").getAsJsonArray("pages");
                message.what = jsonPages.size();

                Log.i("googleSearchPagesCount", String.valueOf(message.what));

                if (!Thread.currentThread().isInterrupted()) {
                    handler.sendMessage(message);
                }

            } catch (Exception e) {
                handler.sendEmptyMessage(ERROR_CODE);
                e.printStackTrace();
            }
        }
    }


}

