package mikhail.yudin.GoogleImageSearcher.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.Toast;
import mikhail.yudin.GoogleImageSearcher.cache.DiskLimitedLruCache;
import mikhail.yudin.GoogleImageSearcher.image.Image;

import java.io.*;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 02.03.14
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */
public class ImageUtils {
    private static final ExecutorService fileSaviourExecutor = Executors.newFixedThreadPool(4);
    private static final ExecutorService imagesLoadExecutor = new ThreadPoolExecutor(5, 5,
            0L, TimeUnit.MILLISECONDS,
            new LifoQueue<Runnable>(50));

    public static void submitToDiskCache(File file, Bitmap bitmap) {
        if (file == null || bitmap == null || bitmap.isRecycled()) {
            Log.w("cacheSubmitting", "Submitting to disk failed.");
        } else {
            fileSaviourExecutor.submit(new FileSaveTask(file, bitmap));
        }
    }

    public static Bitmap downloadThumbBitmap(String url) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(NetworkUtils.getStreamFromUrl(url));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static Bitmap downloadBigBitmap(final File file, String url, final int width, final int height) {
        Bitmap bitmap = null;
        try {
            saveStreamToFile(file, NetworkUtils.getStreamFromUrl(url));
            if (file.exists()) {
                BitmapFactory.Options options = getBitmapOptions(file.getPath(), width, height);
                if (options != null) {
                    bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public static BitmapFactory.Options getBitmapOptions(final String path, final int desiredWidth,
                                                         final int desiredHeight) {
        try {
            //Decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            //The new size we want to scale to
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (options.outWidth / scale / 2 >= desiredWidth && options.outHeight / scale / 2 >= desiredHeight) {
                scale *= 2;
            }
            BitmapFactory.Options correctOptions = new BitmapFactory.Options();
            correctOptions.inSampleSize = scale;
            return correctOptions;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void loadFullImage(final Image image, final ImageView imageView) {
        new LoadFullImageAsyncTask(image, imageView).execute();
    }

    public static void loadThumbImage(final Image image, final ImageView imageView) {
        final ImageLoadTask imageLoadTask = new ImageLoadTask(image, imageView);
        imagesLoadExecutor.submit(imageLoadTask);
    }

    private static class FileSaveTask implements Runnable {

        private final File file;
        private final Bitmap bitmap;

        private FileSaveTask(final File file, final Bitmap bitmap) {
            this.file = file;
            this.bitmap = bitmap;
        }

        @Override
        public void run() {
            //some performance tuning
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file.getPath());
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                }
                DiskLimitedLruCache.getInstance().put(file);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private static class ImageLoadTask implements Runnable {

        private final Image image;
        private final ImageView imageView;

        public ImageLoadTask(final Image image, final ImageView imageView) {
            this.image = image;
            this.imageView = imageView;
        }

        @Override
        public void run() {
            //some performance tuning
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            final Bitmap bm;

            File file = DiskLimitedLruCache.getInstance().get(image.getThumbLocalPath());
            String url = image.getThumbUrl();

            if (!file.exists()) {
                bm = downloadThumbBitmap(url);
                if (bm != null) {
                    submitToDiskCache(file, bm.copy(bm.getConfig(), true));
                }
            } else {
                bm = BitmapFactory.decodeFile(file.getPath());
            }

            new Handler(Looper.getMainLooper()).post(new ImageViewUpdaterTask(bm, imageView));
        }
    }


    private static void saveStreamToFile(final File file, InputStream inputStream) {
        try {
            final OutputStream output = new FileOutputStream(file);
            try {
                try {
                    final byte[] buffer = new byte[1024];
                    int read;

                    while ((read = inputStream.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }

                    output.flush();
                } finally {
                    output.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static class ImageViewUpdaterTask implements Runnable {

        private final Bitmap bitmap;
        private final ImageView imageView;

        private ImageViewUpdaterTask(final Bitmap bitmap, final ImageView imageView) {
            this.bitmap = bitmap;
            this.imageView = imageView;
        }

        @Override
        public void run() {
            //some performance tuning
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            try {
                imageView.setImageBitmap(bitmap);
                imageView.setVisibility(View.VISIBLE);

                AlphaAnimation fadeImage = new AlphaAnimation(0, 1);
                fadeImage.setDuration(1000L);
                fadeImage.setInterpolator(new DecelerateInterpolator());

                imageView.startAnimation(fadeImage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static class LoadFullImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        private final Image image;
        private final ImageView imageView;
        private final Context context;
        private final ProgressDialog progressDialog;

        private LoadFullImageAsyncTask(final Image image, final ImageView imageView) {
            this.image = image;
            this.imageView = imageView;
            this.context = imageView.getContext();
            progressDialog = new ProgressDialog(context);
        }


        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(final Void... params) {
            final Bitmap bm;
            final DiskLimitedLruCache cache = DiskLimitedLruCache.getInstance();

            final File file = cache.get(image.getFullLocalPath());
            String url = image.getUrl();

            //detect screen size to fit image
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);

            if (!file.exists()) {
                bm = downloadBigBitmap(file,
                        url,
                        Math.round(dm.widthPixels / dm.density),
                        Math.round(dm.heightPixels / dm.density));
                if (bm != null) {
                    if (!cache.put(file)) {
                        //Original file is too big to put in cache
                        //trying to put scaled copy
                        file.delete();
                        submitToDiskCache(file, bm.copy(bm.getConfig(), true));
                    }
                }
            } else {
                bm = BitmapFactory.decodeFile(file.getPath());
            }
            return bm;
        }

        @Override
        protected void onPostExecute(final Bitmap bitmap) {
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            } else {
                Toast.makeText(context, "Error occurred during loading the image.", Toast.LENGTH_LONG).show();
            }
            progressDialog.dismiss();
        }
    }

    /**
     * Simple implementation of LIFO Queue. It is needed to loading new elements (@E) first
     * and fit the queue to size when overflow happens.
     * * @param <E>
     */
    private static class LifoQueue<E> extends LinkedBlockingDeque<E> {

        private LifoQueue(final int capacity) {
            super(capacity);
        }

        //Lock-free, but not exact. It is possible sometimes to delete redundant value.
        @Override
        public boolean offer(final E e) {
            if (remainingCapacity() == 0) {
                removeLast();
            }
            return super.offerFirst(e);
        }
    }
}
