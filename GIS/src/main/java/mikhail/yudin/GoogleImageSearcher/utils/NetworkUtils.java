package mikhail.yudin.GoogleImageSearcher.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 02.03.14
 * Time: 22:45
 * To change this template use File | Settings | File Templates.
 */
public class NetworkUtils {

    private static final int HTTP_CONNECTION_TIMEOUT = 30000;
    private static final int BUFFER_SIZE = 8192;
    private static final int CONNECT_TIMEOUT = 5000;
    private static final int READ_TIMEOUT = 10000;
    private static final String ENCODING = "UTF-8";

    private static HttpClient createClient() {
        HttpParams httpParameters = new BasicHttpParams();
        HttpProtocolParams.setContentCharset(httpParameters, ENCODING);
        HttpProtocolParams.setHttpElementCharset(httpParameters, ENCODING);
        HttpConnectionParams.setConnectionTimeout(httpParameters, HTTP_CONNECTION_TIMEOUT);
        return new DefaultHttpClient(httpParameters);
    }

    public static String readAsString(final HttpResponse response) throws IOException {
        InputStream content = response.getEntity().getContent();
        BufferedReader br = null;
        try {
            StringBuilder sb = new StringBuilder();
            br = new BufferedReader(new InputStreamReader(content));
            char[] data = new char[BUFFER_SIZE];
            int i;
            while ((i = br.read(data)) > 0) {
                sb.append(data, 0, i);
            }
            return sb.toString();
        } finally {
            if (br != null) {
                br.close();
            }
            content.close();
        }
    }

    public static HttpResponse sendGetRequest(final String url) throws IOException {
        return createClient().execute(new HttpGet(url));
    }

    public static InputStream getStreamFromUrl(final String url) throws IOException {
        URLConnection connection;
        connection = new URL(url).openConnection();
        connection.setConnectTimeout(CONNECT_TIMEOUT);
        connection.setReadTimeout(READ_TIMEOUT);
        return (InputStream) connection.getContent();
    }
}
